// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSHUD.h"

#include "Kismet/GameplayStatics.h"
#include "TDS_StaminaWidget.h"
#include "TDSInventoryWidget.h"
#include "TDSInteractableItemWidget.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Character/TDSStaminaSystem.h"

void ATDSHUD::BeginPlay(void)
{
    Super::BeginPlay();

    GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ATDSHUD::InitWidgets);
}

void ATDSHUD::InitWidgets()
{
    StaminaWidget = Cast<UTDS_StaminaWidget>(CreateWidget(GetWorld(), StaminaWidgetClass, TEXT("StaminaWidget")));
    StaminaWidget->AddToViewport();
    InventoryWidget = Cast<UTDSInventoryWidget>(CreateWidget(GetWorld(), InventoryWidgetClass, TEXT("InventoryWidget")));
    InventoryWidget->AddToViewport();
    InteractableItemWidget = Cast<UTDSInteractableItemWidget>(CreateWidget(GetWorld(), InteractableItemWidgetClass, TEXT("InteractableItemWidget")));
    InteractableItemWidget->AddToViewport();
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDSInventoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon1;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon2;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon3;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon4;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText1;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText2;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText3;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* AmmoText4;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UBorder* IconBorder1;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UBorder* IconBorder2;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UBorder* IconBorder3;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UBorder* IconBorder4;

	const int WEAPON_SLOT_NUM = 4;

	UPROPERTY(BlueprintReadWrite)
	TArray<class UImage*> IconList;
	UPROPERTY(BlueprintReadWrite)
	TArray<class UTextBlock*> AmmoTextList;
	UPROPERTY(BlueprintReadWrite)
	TArray<class UBorder*> IconBorderList;

	FLinearColor ActiveColor = FLinearColor::Red;
	FLinearColor DefaultColor = FLinearColor::Black;
public:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void UpdateCurrentWeaponIndex(int Ind);
	UFUNCTION()
	void UpdateWeaponIcon(int Ind, const FSoftObjectPath& Path);
	UFUNCTION()
	void UpdateAmmoSlots(class UTDSInventorySystem* Inventory);
};

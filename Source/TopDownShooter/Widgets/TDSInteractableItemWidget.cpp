// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInteractableItemWidget.h"

#include "UMG/Public/Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/TopDownShooterCharacter.h"

void UTDSInteractableItemWidget::NativeConstruct()
{
    Super::NativeConstruct();

    ATopDownShooterCharacter* ch = Cast<ATopDownShooterCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    if (IsValid(ch))
    {
        ch->OnInteractableItemChange.AddDynamic(this, &UTDSInteractableItemWidget::SetText);
    }
}

void UTDSInteractableItemWidget::SetText(const FString& Str)
{
    Text->SetText(FText::FromString(Str));
}

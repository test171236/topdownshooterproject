// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventoryWidget.h"

#include "UMG/Public/Components/Border.h"
#include "UMG/Public/Components/Image.h"
#include "UMG/Public/Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/TopDownShooterCharacter.h"
#include "../Character/TDSInventorySystem.h"

void UTDSInventoryWidget::NativeConstruct()
{
    Super::NativeConstruct();

    IconList.Empty();
    IconList.Add(Icon1);
    IconList.Add(Icon2);
    IconList.Add(Icon3);
    IconList.Add(Icon4);

    AmmoTextList.Empty();
    AmmoTextList.Add(AmmoText1);
    AmmoTextList.Add(AmmoText2);
    AmmoTextList.Add(AmmoText3);
    AmmoTextList.Add(AmmoText4);

    IconBorderList.Empty();
    IconBorderList.Add(IconBorder1);
    IconBorderList.Add(IconBorder2);
    IconBorderList.Add(IconBorder3);
    IconBorderList.Add(IconBorder4);

    ATopDownShooterCharacter* ch = Cast<ATopDownShooterCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    if (IsValid(ch))
    {
        ch->InventorySystem->OnCurrentWeaponIndexUpdate.AddDynamic(this, &UTDSInventoryWidget::UpdateCurrentWeaponIndex);
        UpdateCurrentWeaponIndex(ch->InventorySystem->GetCurrentWeaponIndex());

        ch->InventorySystem->OnUpdateWeaponSlot.AddDynamic(this, &UTDSInventoryWidget::UpdateWeaponIcon);
        for (int i = 0; i < WEAPON_SLOT_NUM; i++)
            if (IsValid(ch->InventorySystem->WeaponList[i]))
                UpdateWeaponIcon(i, ch->InventorySystem->WeaponList[i]->Settings.Icon);
            else
                UpdateWeaponIcon(i, ch->InventorySystem->NoWeapon->Settings.Icon);
        
        ch->InventorySystem->OnUpdateAmmoSlots.AddDynamic(this, &UTDSInventoryWidget::UpdateAmmoSlots);
        UpdateAmmoSlots(ch->InventorySystem);
    }
}

void UTDSInventoryWidget::UpdateCurrentWeaponIndex(int Ind)
{
    if (Ind < 0 || Ind >= WEAPON_SLOT_NUM)
        return;

    for (int i = 0; i < IconBorderList.Num(); i++)
        if (i == Ind)
            IconBorderList[i]->SetBrushColor(ActiveColor);
        else
            IconBorderList[i]->SetBrushColor(DefaultColor);
}

void UTDSInventoryWidget::UpdateWeaponIcon(int Ind, const FSoftObjectPath& Path)
{
    TScriptInterface<ISlateTextureAtlasInterface> Icon(Path.TryLoad());

    IconList[Ind]->SetBrushFromAtlasInterface(Icon);
}

void UTDSInventoryWidget::UpdateAmmoSlots(UTDSInventorySystem* Inventory)
{
    for (int i = 0; i < WEAPON_SLOT_NUM; i++)
    {
        ATDSWeapon* weapon = Inventory->WeaponList[i];
        if (!IsValid(weapon))
            weapon = Inventory->NoWeapon;

        if (weapon->State.CurAmmoTypeIndex >= weapon->Settings.AmmoInfoList.Num())
            AmmoTextList[i]->SetVisibility(ESlateVisibility::Hidden);
        else {
            FString str;
            str = str.Printf(TEXT("%d/%d"), weapon->GetCurrentRounds(),
                Inventory->AmmoAmount[weapon->GetCurrentAmmoType()]);
            AmmoTextList[i]->SetText(FText::FromString(str));
            AmmoTextList[i]->SetVisibility(ESlateVisibility::Visible);
        }
    }
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSPickUpAmmo.h"

#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

#include "../Character/TopDownShooterCharacter.h"

// Sets default values
ATDSPickUpAmmo::ATDSPickUpAmmo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	SetRootComponent(Sphere);
	Sphere->SetCollisionProfileName(TEXT("OverlapAll"));
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));

	MeshList.Empty();
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EAmmoType"), true);
	if (EnumPtr)
		for (uint8 i = 0; i < EnumPtr->GetMaxEnumValue(); i++)
			MeshList.Add((EAmmoType)i, nullptr);
}

// Called when the game starts or when spawned
void ATDSPickUpAmmo::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATDSPickUpAmmo::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	Mesh->SetStaticMesh(MeshList[AmmoType]);
	Mesh->SetRelativeTransform(FTransform());
}

void ATDSPickUpAmmo::Interact(ATopDownShooterCharacter* Player)
{
	Ammount -= Player->PickUpAmmo(AmmoType, Ammount);
	
	if (Ammount <= 0)
		Destroy();
}

FString ATDSPickUpAmmo::GetHUDText() const
{
	return FString("Press E to take");
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSInteractable.h"
#include "TDSPickUpWeapon.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSPickUpWeapon : public AActor, public ITDSInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSPickUpWeapon();

protected:
	UPROPERTY(EditAnywhere)
	class USphereComponent* Sphere;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform &Transform) override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;
	virtual void BeginDestroy() override;

	UPROPERTY()
	class ATDSWeapon* Weapon = nullptr;

	UPROPERTY(EditInstanceOnly)
	FName StartWeaponName;

	UPROPERTY(EditDefaultsOnly)
	class UDataTable* WeaponInfoTable = nullptr;
public:	
	virtual void Interact(class ATopDownShooterCharacter* Player) override;
	virtual FString GetHUDText() const override;

	void InitWeapon(class ATDSWeapon* newWeapon);
};

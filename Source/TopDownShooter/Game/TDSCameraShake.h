// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MatineeCameraShake.h"
#include "TDSCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UTDSCameraShake : public UMatineeCameraShake
{
	GENERATED_BODY()
	
};

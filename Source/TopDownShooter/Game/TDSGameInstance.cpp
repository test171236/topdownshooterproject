// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& WeaponInfo)
{
    FWeaponInfo* info = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "");
    if (info)
    {
        WeaponInfo = *info;
        return true;
    }
    return false;
}

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLib/TDSTypes.h"
#include "Weapon/TDSWeapon.h"
#include "../Items/TDSInteractable.h"
#include "TopDownShooterCharacter.generated.h"

class UTDSCameraShake;
class UTDSStaminaSystem;

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATopDownShooterCharacter();

	virtual void BeginPlay() override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

protected:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ReloadStart(ATDSWeapon* Weapon);
	void ReloadStart_Implementation(ATDSWeapon* Weapon);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void WeaponFireStart(ATDSWeapon* Weapon);
	void WeaponFireStart_Implementation(ATDSWeapon* Weapon);

	UFUNCTION(BlueprintCallable)
	void CameraShakeEvent() const;

	UFUNCTION(BlueprintCallable)
	void SetCameraHeight(float newCameraHeight);
	UFUNCTION(BlueprintCallable)
	void SetCameraRot(FRotator newCameraRot);
	UFUNCTION(BlueprintCallable)
	void SetCameraFlow(float newCameraLagSpeed, bool newIsFlow = true);

private:
	float ComputeChracterYaw(float DeltaSeconds);

	void SetupCurrentWeapon();
	void RemoveCurrentWeapon();

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	TSubclassOf<UTDSCameraShake> CameraShakeClass;

	UPROPERTY()
	TScriptInterface<ITDSInteractable> InteractableItem;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ATDSPickUpWeapon> DropWeaponClass;

	float AxisX = 0.f, AxisY = 0.f;

	float CurYaw, TargetYaw;

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteractableItemChange, const FString&, Str);
	UPROPERTY()
	FOnInteractableItemChange OnInteractableItemChange;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	UTDSStaminaSystem* StaminaSystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	class UTDSInventorySystem* InventorySystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool IsAimEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool IsWalkEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool IsSprintEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	/*UPROPERTY(BlueprintReadWrite)
	ATDSWeapon *CurWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName StartWeaponName;*/

	UFUNCTION()
	void InputAxisX(float val);
	UFUNCTION()
	void InputAxisY(float val);
	UFUNCTION()
	void FireStart();
	UFUNCTION()
	void FireEnd();
	UFUNCTION()
	void MovementTick(float DeltaSeconds);
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void UpdateMovementState();

	UFUNCTION(BlueprintCallable)
	void ManualReload();

	UFUNCTION(BlueprintCallable)
	void ChangeCurrentWeapon(int Index);

	UFUNCTION(BlueprintImplementableEvent)
	void SetWeaponAnim(ATDSWeapon *Weapon);

	UFUNCTION(BlueprintCallable)
	void Interact();

	UFUNCTION(BlueprintCallable)
	int PickUpAmmo(EAmmoType Type, int Ammount);
	
	UFUNCTION(BlueprintCallable)
	bool PickUpWeapon(ATDSWeapon* Weapon);
	UFUNCTION(BlueprintCallable)
	bool DropWeapon();
};


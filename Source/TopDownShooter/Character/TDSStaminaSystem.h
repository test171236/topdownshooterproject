// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSStaminaSystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UTDSStaminaSystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	float MaxStamina = 100.f;
	float CurStamina = 100.f;
	float StaminaRecoverTimeDelay = 2.f;
	float StaminaRecoverSpeed = 10.f;

	float LastStaminaSpendTime;

	// Sets default values for this component's properties
	UTDSStaminaSystem();

	bool SpendStamina(float Delta);

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStaminaUpdate, float, Percent);
	FOnStaminaUpdate OnStaminaUpdate;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurrentStaminaPercent() const;
};

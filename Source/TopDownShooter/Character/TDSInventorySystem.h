// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../FuncLib/TDSTypes.h"
#include "TDSInventorySystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNSHOOTER_API UTDSInventorySystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCurrentWeaponIndexUpdate, int, Ind);
	UPROPERTY()
	FOnCurrentWeaponIndexUpdate OnCurrentWeaponIndexUpdate;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int, Ind, const FSoftObjectPath&, Path);
	UPROPERTY()
	FOnUpdateWeaponSlot OnUpdateWeaponSlot;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateAmmoSlots, UTDSInventorySystem*, InventorySystem);
	UPROPERTY()
	FOnUpdateAmmoSlots OnUpdateAmmoSlots;

	UTDSInventorySystem();

	const int WEAPON_SLOT_NUM = 4;

	UPROPERTY(BlueprintReadWrite)
	class ATDSWeapon* NoWeapon;

	UPROPERTY(EditDefaultsOnly)
	TArray<FName> StartWeaponList;

	UPROPERTY(BlueprintReadWrite)
	TArray<class ATDSWeapon *> WeaponList;

	UPROPERTY(BlueprintReadWrite)
	TMap<EAmmoType, int> AmmoAmount;

protected:
	UPROPERTY(BlueprintReadWrite)
	int CurrentWeaponIndex = 0;

	virtual void BeginPlay() override;
	void InitWeapon(class ATDSWeapon* &Weapon, FName WeaponName);

	UFUNCTION()
	void UpdateAmmoSlots(class ATDSWeapon* Weapon);
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ATDSWeapon* GetCurrentWeapon() const;

	UFUNCTION(BlueprintCallable)
	void SetCurrentWeaponIndex(int Ind);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentWeaponIndex() const { return CurrentWeaponIndex; }

	void ReloadWeapon();
	void SetWeapon(int Ind, ATDSWeapon* Weapon);
	ATDSWeapon* RemoveWeapon(int Ind);

	int PickUpAmmo(EAmmoType Type, int Ammount);
};

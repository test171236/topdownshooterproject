// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStaminaSystem.h"

// Sets default values for this component's properties
UTDSStaminaSystem::UTDSStaminaSystem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

bool UTDSStaminaSystem::SpendStamina(float Delta)
{
	if (CurStamina >= Delta)
	{
		CurStamina -= Delta;
		LastStaminaSpendTime = GetWorld()->GetTimeSeconds();
		OnStaminaUpdate.Broadcast(GetCurrentStaminaPercent());
		return true;
	}
	return false;
}


// Called when the game starts
void UTDSStaminaSystem::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UTDSStaminaSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (CurStamina < MaxStamina && GetWorld()->GetTimeSeconds() - LastStaminaSpendTime > StaminaRecoverTimeDelay)
	{
		CurStamina = fminf(CurStamina + StaminaRecoverSpeed * DeltaTime, MaxStamina);
		OnStaminaUpdate.Broadcast(GetCurrentStaminaPercent());
	}
}

float UTDSStaminaSystem::GetCurrentStaminaPercent()	const
{
	return CurStamina / MaxStamina;
}


// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Game/TDSCameraShake.h"
#include "TDSStaminaSystem.h"
#include "TDSInventorySystem.h"
#include "../Game/TDSGameInstance.h"
#include "../Items/TDSPickUpWeapon.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	//CameraBoom->TargetArmLength = 800.f;
	//CameraBoom->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	//CameraBoom->bEnableCameraLag = true;
	CameraBoom->CameraLagSpeed = 3.0f;
	//CameraBoom->bDrawDebugLagMarkers = true;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	StaminaSystem = CreateDefaultSubobject<UTDSStaminaSystem>("StaminsSystem");
	InventorySystem = CreateDefaultSubobject<UTDSInventorySystem>("InventorySystem");
}

void ATopDownShooterCharacter::ChangeCurrentWeapon(int Index)
{
	RemoveCurrentWeapon();
	InventorySystem->SetCurrentWeaponIndex(Index);
	SetupCurrentWeapon();
}


void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	ChangeCurrentWeapon(0);
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_GameTraceChannel1, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(class UInputComponent* newInputComponent)
{
	Super::SetupPlayerInputComponent(newInputComponent);

	InputComponent = newInputComponent;

	InputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);
	InputComponent->BindAction(TEXT("FireAction"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::FireStart);
	InputComponent->BindAction(TEXT("FireAction"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::FireEnd);
	InputComponent->BindAction(TEXT("ReloadAction"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::ManualReload);
}

void ATopDownShooterCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor->GetClass()->ImplementsInterface(UTDSInteractable::StaticClass()))
	{
		InteractableItem = OtherActor;
		OnInteractableItemChange.Broadcast(InteractableItem->GetHUDText());
	}
}

void ATopDownShooterCharacter::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if (OtherActor == InteractableItem.GetObject())
	{
		InteractableItem = NULL;
		OnInteractableItemChange.Broadcast(TEXT(""));
	}
}

void ATopDownShooterCharacter::ManualReload()
{
	InventorySystem->ReloadWeapon();
}

void ATopDownShooterCharacter::SetCameraHeight(float newCameraHeight)
{
	CameraBoom->TargetArmLength = newCameraHeight;
}

void ATopDownShooterCharacter::SetCameraRot(FRotator newCameraRot)
{
	CameraBoom->SetRelativeRotation(newCameraRot);
}

void ATopDownShooterCharacter::SetCameraFlow(float newCameraLagSpeed, bool newIsFlow)
{
	CameraBoom->bEnableCameraLag = newIsFlow;
	CameraBoom->CameraLagSpeed = newCameraLagSpeed;
}

void ATopDownShooterCharacter::SetupCurrentWeapon()
{
	ATDSWeapon* CurWeapon = InventorySystem->GetCurrentWeapon();

	FAttachmentTransformRules rule(EAttachmentRule::SnapToTarget, false);
	CurWeapon->AttachToComponent(GetMesh(), rule, FName("WeaponSocketRightHand"));
	CurWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::ReloadStart);
	CurWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponFireStart);
	SetWeaponAnim(CurWeapon);
	CurWeapon->Mesh->SetVisibility(true);
}

void ATopDownShooterCharacter::RemoveCurrentWeapon()
{
	ATDSWeapon* CurWeapon = InventorySystem->GetCurrentWeapon();

	CurWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	CurWeapon->OnWeaponReloadStart.Clear();
	CurWeapon->OnWeaponFireStart.Clear();
	CurWeapon->Mesh->SetVisibility(false);
}

void ATopDownShooterCharacter::InputAxisX(float val)
{
	AxisX = val;
}

void ATopDownShooterCharacter::InputAxisY(float val)
{
	AxisY = val;
}

float ATopDownShooterCharacter::ComputeChracterYaw(float DeltaSeconds)
{
	if (MovementState == EMovementState::Sprint_State && (abs(AxisX) > 0.1f || abs(AxisY) > 0.1f))
		TargetYaw = atan2f(AxisY, AxisX) * 180.f / PI;
	else {
		APlayerController* my_controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (IsValid(my_controller))
		{
			FHitResult hr;
															  
			my_controller->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, hr);
			FRotator rot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), hr.Location);
			TargetYaw = rot.Yaw;
		}
	}

	const float CharacterRotateSpeedDegreeInSecond = 360.f;

	float delta = fmod(TargetYaw - CurYaw, 360.f);
	if (delta > 180.f)
		delta -= 360.f;
	else if (delta < -180.f)
		delta += 360.f;

	float add_delta = CharacterRotateSpeedDegreeInSecond * DeltaSeconds * UKismetMathLibrary::SignOfFloat(delta);

	if (abs(add_delta) <= abs(delta))
		CurYaw += add_delta;
	else
		CurYaw = TargetYaw;

	return CurYaw;
}

void ATopDownShooterCharacter::MovementTick(float DeltaSeconds)
{
	if (MovementState == EMovementState::Sprint_State && (abs(AxisX) > 0.1f || abs(AxisY) > 0.1f))
		if (!StaminaSystem->SpendStamina(DeltaSeconds * 20.f))
		{
			IsSprintEnable = false;
			UpdateMovementState();
		}

	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

	SetActorRotation(FQuat(FRotator(0.f, ComputeChracterYaw(DeltaSeconds), 0.f)));
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float res_speed = 600.f;
	
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		res_speed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		res_speed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		res_speed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		res_speed = MovementInfo.SprintSpeed;
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = res_speed;
}

void ATopDownShooterCharacter::FireStart()
{
	if (InventorySystem->GetCurrentWeapon())
		InventorySystem->GetCurrentWeapon()->SetFiring(true);
}

void ATopDownShooterCharacter::FireEnd()
{
	if (InventorySystem->GetCurrentWeapon())
		InventorySystem->GetCurrentWeapon()->SetFiring(false);
}

void ATopDownShooterCharacter::UpdateMovementState()
{
	if (IsSprintEnable)
		MovementState = EMovementState::Sprint_State;
	else if (IsAimEnable)
		MovementState = EMovementState::Aim_State;
	else if (IsWalkEnable)
		MovementState = EMovementState::Walk_State;
	else
		MovementState = EMovementState::Run_State;

	CharacterUpdate();
}

void ATopDownShooterCharacter::ReloadStart_Implementation(ATDSWeapon* Weapon)
{
	PlayAnimMontage(InventorySystem->GetCurrentWeapon()->Settings.ReloadAnim);
}

void ATopDownShooterCharacter::WeaponFireStart_Implementation(ATDSWeapon* Weapon)
{
	if(IsAimEnable)
		PlayAnimMontage(InventorySystem->GetCurrentWeapon()->Settings.FireAimAnim);
	else
		PlayAnimMontage(InventorySystem->GetCurrentWeapon()->Settings.FireWalkAnim);
}

void ATopDownShooterCharacter::CameraShakeEvent(void) const
{
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayWorldCameraShake(GetWorld(), CameraShakeClass, 
		TopDownCameraComponent->GetComponentLocation(), 1.f, 1.f, 1.f);
}

void ATopDownShooterCharacter::Interact(void)
{
	if (InteractableItem)
		InteractableItem->Interact(this);
}

int ATopDownShooterCharacter::PickUpAmmo(EAmmoType Type, int Ammount)
{
	return InventorySystem->PickUpAmmo(Type, Ammount);
}

bool ATopDownShooterCharacter::PickUpWeapon(ATDSWeapon* Weapon)
{
	if (IsValid(InventorySystem->WeaponList[InventorySystem->GetCurrentWeaponIndex()]))
		return false;

	InventorySystem->SetWeapon(InventorySystem->GetCurrentWeaponIndex(), Weapon);
	ChangeCurrentWeapon(InventorySystem->GetCurrentWeaponIndex());

	return true;
}

bool ATopDownShooterCharacter::DropWeapon()
{
	if (!IsValid(InventorySystem->WeaponList[InventorySystem->GetCurrentWeaponIndex()]))
		return false;

	ATDSPickUpWeapon* item = GetWorld()->SpawnActor<ATDSPickUpWeapon>(DropWeaponClass, 
		FTransform(GetActorLocation()));

	if (IsValid(item))
	{
		item->InitWeapon(InventorySystem->RemoveWeapon(InventorySystem->GetCurrentWeaponIndex()));
		ChangeCurrentWeapon(InventorySystem->GetCurrentWeaponIndex());
		return true;
	}
	
	return false;
}
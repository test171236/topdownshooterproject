// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSWeapon.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "DrawDebugHelpers.h"
#include "Niagara/Public/NiagaraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDSProjectile.h"

// Sets default values
ATDSWeapon::ATDSWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));
	Mesh->SetupAttachment(Root);
	Mesh->SetVisibility(false);

	ShootLoc = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLoc"));
	ShootLoc->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void ATDSWeapon::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATDSWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATDSWeapon::SetFiring(bool nIsFiring)
{
	State.IsFiring = nIsFiring;
	if (State.IsFiring && !State.IsReloading)
	{
		float time_remain = 1.f / Settings.RateOfFire - (GetWorld()->TimeSeconds - State.TimeOfLastShot);
		float init_delta_time = (time_remain <= 0.f ? 0.f : time_remain);

		GetWorld()->GetTimerManager().SetTimer(
			FiringTimerHandle,
			this,
			&ATDSWeapon::Fire,
			1.f / Settings.RateOfFire,
			true, 
			init_delta_time);
	}
	else
		GetWorld()->GetTimerManager().ClearTimer(FiringTimerHandle);
}

void ATDSWeapon::Fire()
{
	FVector loc = ShootLoc->GetComponentLocation();
	FRotator rot = GetOwner()->GetActorRotation();

	if (State.CurRounds <= 0)
	{
		//ReloadStart();
		SetFiring(false);
		return;
	}

	if (Settings.Type == EWeaponType::ProjectileType)
	{
		ATDSProjectile* proj = GetWorld()->SpawnActorDeferred<ATDSProjectile>(Settings.AmmoInfoList[State.CurAmmoTypeIndex].ProjectileInfo.ProjectileClass.Get(),
			FTransform(rot, loc), (AActor*)this, (APawn*)nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

		if (IsValid(proj))
		{
			proj->InitProjectile(Settings.AmmoInfoList[State.CurAmmoTypeIndex].ProjectileInfo);
			proj->FinishSpawning(FTransform(rot, loc));
		}
	}
	else if (Settings.Type == EWeaponType::HitScanType)
	{
		for (int i = 0; i < Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.HitNum; i++)
		{
			FHitResult fire_hr;
			FVector fire_start = loc;
			FVector fire_dir = (rot + FRotator(Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Dispersion * (2 * FMath::FRand() - 1),
				Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Dispersion * (2 * FMath::FRand() - 1),
				Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Dispersion * (2 * FMath::FRand() - 1))).Vector();
			FVector fire_end = fire_start + fire_dir * Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Range;
			FCollisionQueryParams params;

			GetWorld()->LineTraceSingleByChannel(
				fire_hr,
				fire_start,
				fire_end,
				ECC_GameTraceChannel3
			);

			/*UKismetSystemLibrary::LineTraceSingle(
				GetWorld(),
				fire_start,
				fire_end,
				ECC_GameTraceChannel3,
				false,
				TArray<AActor*>(),
				EDrawDebugTrace::ForOneFrame,
				fire_hr,
				true
			);*/

			/*DrawDebugLine(GetWorld(),
				fire_start,
				fire_hr.bBlockingHit ? fire_hr.ImpactPoint : fire_end,
				FColor::Green,
				false,
				2);*/

			UNiagaraComponent* nc = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),
				Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.Trail,
				GetActorLocation(),
				FRotator::ZeroRotator,
				FVector(1.f),
				true,
				false,
				ENCPoolMethod::AutoRelease,
				true);

			nc->SetVectorParameter(FName("EndPoint"), 
				fire_hr.bBlockingHit ? fire_hr.ImpactPoint : fire_end);

			nc->Activate();

			if (fire_hr.bBlockingHit)
			{
				float damage = FMath::Lerp(Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.MaxDamage,
					Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.MinDamage,
					fire_hr.Time);

				fire_hr.Actor->TakeDamage(damage, FPointDamageEvent(), nullptr, this);

				FTransform particle_trans;
				particle_trans.SetLocation(fire_hr.ImpactPoint);
				particle_trans.SetRotation(
					UKismetMathLibrary::MakeRotFromZ(fire_hr.ImpactNormal).Quaternion());

				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Settings.AmmoInfoList[State.CurAmmoTypeIndex].HitScanInfo.HitParticle,
					particle_trans, true, EPSCPoolMethod::AutoRelease, true);

			}
		}
	}

	SpawnRound();
	State.CurRounds--;
	OnCurRoundsChanged.Broadcast(this);
	OnWeaponFireStart.Broadcast(this);
	State.TimeOfLastShot = GetWorld()->TimeSeconds;
}

void ATDSWeapon::InitWeapon(const FWeaponInfo& nSettings)
{
	Settings = nSettings;

	State.CurRounds = nSettings.MaxRounds;
	OnCurRoundsChanged.Broadcast(this);
}

int ATDSWeapon::GetCurrentRounds() const
{
	return State.CurRounds;
}

int ATDSWeapon::GetMaxRounds() const
{
	return Settings.MaxRounds;
}

void ATDSWeapon::ReloadStart(int* newAmmoCache)
{
	if (!State.IsReloading)
	{
		State.IsReloading = true;
		SetFiring(false);
		AmmoCache = newAmmoCache;

		GetWorld()->GetTimerManager().SetTimer(ReloadTimerHandle,
			this,
			&ATDSWeapon::ReloadEnd,
			Settings.ReloadTime);
		
		OnWeaponReloadStart.Broadcast(this);
	}
}

void ATDSWeapon::ReloadEnd()
{
	State.IsReloading = false;

	if (AmmoCache != nullptr)
	{
		int delta = FMath::Min(Settings.MaxRounds - State.CurRounds, *AmmoCache);

		State.CurRounds += delta;
		*AmmoCache -= delta;
		AmmoCache = nullptr;
		OnCurRoundsChanged.Broadcast(this);
	}
}

UStaticMesh* ATDSWeapon::GetMagazine_Implementation() const
{
	return nullptr;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "../../FuncLib/TDSTypes.h"
#include "TDSWeapon.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATDSWeapon : public AActor
{
	GENERATED_BODY()
	
	int* AmmoCache = nullptr;
public:	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, ATDSWeapon*, Weapon);
	FOnWeaponReloadStart OnWeaponReloadStart;
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, ATDSWeapon*, Weapon);
	FOnWeaponFireStart OnWeaponFireStart;
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCurRoundsChanged, ATDSWeapon*, Weapon);
	FOnCurRoundsChanged OnCurRoundsChanged;

	// Sets default values for this actor's properties
	ATDSWeapon();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	USceneComponent* Root = nullptr;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UStaticMeshComponent* Mesh = nullptr;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UArrowComponent* ShootLoc = nullptr;
	UPROPERTY(BlueprintReadOnly)
	struct FWeaponInfo Settings;

	UPROPERTY(BlueprintReadWrite)
	FDynamicWeaponInfo State;

	FTimerHandle FiringTimerHandle;
	FTimerHandle ReloadTimerHandle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetFiring(bool nIsFiring);
	void Fire();

	void InitWeapon(const FWeaponInfo &nSettings);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentRounds() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetMaxRounds() const;

	void ReloadStart(int* newAmmoCache);
	void ReloadEnd();

	UFUNCTION(BlueprintNativeEvent, BlueprintPure)
	UStaticMesh* GetMagazine() const;
	UStaticMesh* GetMagazine_Implementation() const;
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetReloadingMesh();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetNormalMesh();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SpawnEjectedMagazine();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SpawnRound();

	EAmmoType GetCurrentAmmoType() const { return (State.CurAmmoTypeIndex >= Settings.AmmoInfoList.Num())
													? EAmmoType::RifleAmmo
													: Settings.AmmoInfoList[State.CurAmmoTypeIndex].Type; }

};

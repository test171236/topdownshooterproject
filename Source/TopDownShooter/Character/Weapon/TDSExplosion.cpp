// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSExplosion.h"

#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

ATDSExplosion::ATDSExplosion()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(FName("Root"));
}

void ATDSExplosion::BeginPlay()
{
	Super::BeginPlay();
	
	if (IsDebug)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionInfo.MaxDamageRange, 32, FColor::Red, false, 3);
		DrawDebugSphere(GetWorld(), GetActorLocation(), (ExplosionInfo.MaxDamageRange + ExplosionInfo.DamageRange) / 2, 32, FColor::Orange, false, 3);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ExplosionInfo.DamageRange, 32, FColor::Yellow, false, 3);
	}

	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ExplosionInfo.MaxDamage,
		0,
		GetActorLocation(),
		ExplosionInfo.MaxDamageRange,
		ExplosionInfo.DamageRange,
		1.f,
		UDamageType::StaticClass(),
		TArray<AActor*>(),
		this,
		nullptr,
		ECC_Visibility);

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), 
		ExplosionInfo.ExplosionParticle,
		FTransform(GetActorLocation()), true,
		EPSCPoolMethod::AutoRelease, true);

	Destroy();
}

void ATDSExplosion::InitExplosion(const FExplosionInfo& ExpInf, const FHitResult &HR)
{
	ExplosionInfo = ExpInf;
	HitRes = HR;
}


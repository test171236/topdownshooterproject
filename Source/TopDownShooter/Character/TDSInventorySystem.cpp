// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventorySystem.h"

#include "../Game/TDSGameInstance.h"
#include "Weapon/TDSWeapon.h"

UTDSInventorySystem::UTDSInventorySystem()
{
	PrimaryComponentTick.bCanEverTick = false;

	WeaponList.Empty();
	WeaponList.AddZeroed(WEAPON_SLOT_NUM);

	AmmoAmount.Empty();
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EAmmoType"), true);
	if (EnumPtr)
		for (uint8 i = 0; i < EnumPtr->GetMaxEnumValue(); i++)
			AmmoAmount.Add((EAmmoType)i, 0);
}


void UTDSInventorySystem::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(NoWeapon, TEXT("NoWeapon"));

	for (int i = 0; i < (StartWeaponList.Num() < WEAPON_SLOT_NUM ? StartWeaponList.Num() : WEAPON_SLOT_NUM); i++)
		InitWeapon(WeaponList[i], StartWeaponList[i]);
}

void UTDSInventorySystem::InitWeapon(ATDSWeapon*& Weapon, FName WeaponName)
{
	if (!IsValid(GetOwner()))
		return;

	UTDSGameInstance* inst = Cast<UTDSGameInstance>(GetOwner()->GetGameInstance());
	FWeaponInfo info;

	if (inst && inst->GetWeaponInfoByName(WeaponName, info))
	{
		if (IsValid(Weapon))
			Weapon->Destroy();

		FActorSpawnParameters asp;
		asp.Owner = GetOwner();
		asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		Weapon = GetWorld()->SpawnActor<ATDSWeapon>(info.WeaponClass, asp);
		if (Weapon)
		{
			Weapon->InitWeapon(info);
			Weapon->OnCurRoundsChanged.AddDynamic(this, &UTDSInventorySystem::UpdateAmmoSlots);

			/*FAttachmentTransformRules rule(EAttachmentRule::SnapToTarget, false);
			CurWeapon->AttachToComponent(GetMesh(), rule, FName("WeaponSocketRightHand"));
			CurWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::ReloadStart);
			CurWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponFireStart);
			SetWeaponAnim(CurWeapon);*/
		}
	}
}

void UTDSInventorySystem::UpdateAmmoSlots(ATDSWeapon* Weapon)
{
	OnUpdateAmmoSlots.Broadcast(this);
}

ATDSWeapon* UTDSInventorySystem::GetCurrentWeapon() const
{
	if (IsValid(WeaponList[CurrentWeaponIndex]))
		return WeaponList[CurrentWeaponIndex];
	else
		return NoWeapon;
}

void UTDSInventorySystem::SetCurrentWeaponIndex(int Ind)
{
	CurrentWeaponIndex = Ind;

	OnCurrentWeaponIndexUpdate.Broadcast(Ind);
}

void UTDSInventorySystem::ReloadWeapon()
{
	if (GetCurrentWeapon()->GetCurrentRounds() < GetCurrentWeapon()->Settings.MaxRounds 
			&& AmmoAmount[GetCurrentWeapon()->GetCurrentAmmoType()] > 0)
		GetCurrentWeapon()->ReloadStart(&AmmoAmount[GetCurrentWeapon()->GetCurrentAmmoType()]);
}

void UTDSInventorySystem::SetWeapon(int Ind, ATDSWeapon* Weapon)
{
	if (IsValid(WeaponList[Ind]))
		WeaponList[Ind]->Destroy();

	WeaponList[Ind] = Weapon;
	Weapon->SetOwner(GetOwner());
	Weapon->OnCurRoundsChanged.Clear();
	Weapon->OnCurRoundsChanged.AddDynamic(this, &UTDSInventorySystem::UpdateAmmoSlots);
	OnUpdateWeaponSlot.Broadcast(Ind, Weapon->Settings.Icon);
	OnUpdateAmmoSlots.Broadcast(this);
}

ATDSWeapon* UTDSInventorySystem::RemoveWeapon(int Ind)
{
	if (!IsValid(WeaponList[Ind]))
		return nullptr;

	ATDSWeapon* Weapon = WeaponList[Ind];
	WeaponList[Ind] = nullptr;
	Weapon->OnCurRoundsChanged.Clear();
	Weapon->OnWeaponReloadStart.Clear();
	Weapon->OnWeaponFireStart.Clear();
	OnUpdateWeaponSlot.Broadcast(Ind, NoWeapon->Settings.Icon);
	OnUpdateAmmoSlots.Broadcast(this);

	return Weapon;
}

int UTDSInventorySystem::PickUpAmmo(EAmmoType Type, int Ammount)
{
	AmmoAmount[Type] += Ammount;
	OnUpdateAmmoSlots.Broadcast(this);

	return Ammount;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraCommon.h"
#include "Animation/BlendSpace1D.h"
#include "TDSTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Aim_State UMETA(DisplayName = "Aim State"),
    Walk_State UMETA(DisplayName = "Walk State"),
    Run_State UMETA(DisplayName = "Run State"),
    Sprint_State UMETA(DisplayName = "Sprint State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeed = 300.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float WalkSpeed = 200.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeed = 700.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float SprintSpeed = 1200.f;
};

UCLASS()
class TOPDOWNSHOOTER_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

};

UENUM(BlueprintType)
enum class EAmmoType : uint8 {
    RifleAmmo UMETA(DisplayName = "Rifle Ammo"),
    ShotgunAmmo UMETA(DisplayName = "Shotgun Ammo"),
    GranadeLauncherAmmo UMETA(DisplayName = "Granade Launcher Ammo"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8 {
    ProjectileType UMETA(DisplayName = "Projectile Type"),
    HitScanType UMETA(DisplayName = "HitScan Type"),
};

class ATDSProjectile;

USTRUCT(BlueprintType)
struct FHitScanInfo
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxDamage;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MinDamage;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Range;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Dispersion;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int HitNum;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UNiagaraSystem* Trail;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UParticleSystem* HitParticle;
};

USTRUCT(BlueprintType)
struct FExplosionInfo
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class ATDSExplosion> ExplosionClass;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxDamageRange;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float DamageRange;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxDamage;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UParticleSystem* ExplosionParticle;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float LifeTime;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Speed;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxDamage;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxDamageRange;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MinDamage;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MinDamageRange;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<ATDSProjectile> ProjectileClass;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UNiagaraSystem* Trail;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UParticleSystem* HitParticle;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool IsExplosive;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FExplosionInfo ExplosionInfo;
};

USTRUCT(BlueprintType)
struct FAmmoInfo
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EAmmoType Type;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FProjectileInfo ProjectileInfo;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FHitScanInfo HitScanInfo;
};

class ATDSWeapon;

USTRUCT(BlueprintType)
struct FWeaponInfo: public FTableRowBase
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EWeaponType Type;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float RateOfFire;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float ReloadTime;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FAmmoInfo> AmmoInfoList;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<ATDSWeapon> WeaponClass;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int MaxRounds;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UAnimMontage* ReloadAnim;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UAnimMontage* FireAimAnim;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UAnimMontage* FireWalkAnim;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UBlendSpace1D* UpperBlendSpaceAim;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UBlendSpace1D* UpperBlendSpaceWalk;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MetaClass = "ISlateTextureAtlasInterface"))
    FSoftObjectPath Icon; //ISlateTextureAtlasInterface
};

USTRUCT(BlueprintType)
struct FDynamicWeaponInfo
{
    GENERATED_BODY()
public:
    UPROPERTY(BlueprintReadWrite)
    int CurRounds;
    UPROPERTY(BlueprintReadWrite)
    bool IsFiring = false;
    UPROPERTY(BlueprintReadWrite)
    float TimeOfLastShot = -1000.f;
    UPROPERTY(BlueprintReadWrite)
    bool IsReloading = false;
    UPROPERTY(BlueprintReadWrite)
    int CurAmmoTypeIndex = 0;
};